const symbols = [
    'conch-shell',
    'dharma-wheel',
    'endless-knot',
    'golden-fish-pair',
    'lotus-flower',
    'parasol',
    'treasure-vase',
    'victory-banner',
];

export default {
    symbols
}
